package ru.sberbank.Figure;

public class ArrayFigure {
    private Figure[] arrayF;

    public ArrayFigure(int aLength)
    {
        this.arrayF = new Figure[aLength];
    }

    /**
     * ����� ���������� ��������� ������� �����
     * @param element ������ ��� ��������� � ������
     * @param i ������ �������� �������
     */
    public void setElement(int i, Figure element)
    {
        this.arrayF[i] = element;
    }

    /**
     * ����� ��������� ��������� ������� �����
     * @param i ������ �������� �������
     */
    public Figure getElement(int i)
    {
        return this.arrayF[i];
    }

    /**
     * ����� ��������� ������� �������
     */
    public int getSize()
    {
        return this.arrayF.length;
    }
}
