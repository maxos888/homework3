package ru.sberbank.Figure;

public class Circle extends Figure {
    public Circle(String nameFigure, float radius)
    {
        super(nameFigure);
        this.radius = radius;
    }

    private float radius;
    private final float PI = 3.14f;

    /**
     * Переопределяет метод вычисления площади для круга
     */
    public double getArea()
    {
        area = PI * Math.pow(radius, 2);
        return area;
    }
}

