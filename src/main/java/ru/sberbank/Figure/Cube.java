package ru.sberbank.Figure;

public class Cube extends Square {
    public Cube(String nameFigure, float sideLength)
    {
        super(nameFigure);
        this.nameFigure = nameFigure;
        this.sideLength = sideLength;
    }

    private double volume;

    /**
     * Вычисляет объем куба
     */
    public double getVolume()
    {
        volume = Math.pow(sideLength, 3);
        return volume;
    }

    /**
     * Выводит на экран результаты выполнения метода {@link Cube#getVolume}
     */
    public void printVolume()
    {
        System.out.print(getVolume());
    }
}

