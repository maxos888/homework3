package ru.sberbank.Figure;

import ru.sberbank.Vector.Vector3D;

public class Figure {
    public Figure(String nameFigure)
    {
        this.area = 0;
        this.nameFigure = nameFigure;
    }
    public Figure() {}

    public String nameFigure;
    public double area;

    /**
     * Вычисляет площадь некой фигуры
     */
    public double getArea()
    {
        return area;
    }

    /**
     * Выводит на экран результаты выполнения метода {@link Figure#getArea}
     */
    public void printArea()
    {
        System.out.print(getArea());
    }

    /**
     * Выводит на экран название фигуры
     */
    public void printName()
    {
        System.out.print(" " + nameFigure + " is ");
    }

    /**
     * Выводит на экран объем фигуры
     */
    public void printVolume() {}
}
