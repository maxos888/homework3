package ru.sberbank.Figure;

public class Main
{
    public static void main( String[] args )
    {
        String nameCube = "Our Cube";

        Figure ourFigure = new Figure("Some Figure");
        Square ourSquare = new Square("Our Square", 2f);
        Circle ourCircle = new Circle("Our Circle", 4f);
        Cube ourCube = new Cube(nameCube, 5f);
        ArrayFigure ourArrayFigure = new ArrayFigure(4);

        ourArrayFigure.setElement(0, ourFigure);
        ourArrayFigure.setElement(1, ourSquare);
        ourArrayFigure.setElement(2, ourCircle);
        ourArrayFigure.setElement(3, ourCube);

        for (int i = 0; i < ourArrayFigure.getSize(); i++)
        {
            if (ourArrayFigure.getElement(i).nameFigure.equals(nameCube))
            {
                System.out.print("Volume of");
                ourArrayFigure.getElement(i).printName();
                ourArrayFigure.getElement(i).printVolume();
            }
            else
            {
                System.out.print("Area of");
                ourArrayFigure.getElement(i).printName();
                ourArrayFigure.getElement(i).printArea();
            }
            System.out.println("");
        }
    }
}