package ru.sberbank.Figure;

public class Square extends Figure {
    public Square(String nameFigure, float sideLength)
    {
        super(nameFigure);
        this.sideLength = sideLength;
    }
    public Square(String nameFigure)
    {
        super();
    }

    public float sideLength;

    /**
     * Переопределяет метод вычисления площади для квадрата
     */
    public double getArea()
    {
        area = Math.pow(sideLength, 2);
        return area;
    }
}

