package ru.sberbank.Vector;

public class Main {
    public static void main( String[] args )
    {
        Vector3D vector1 = new Vector3D(12,-25,31);
        Vector3D vector2 = new Vector3D(40,51,-3);
        Vector3D vectorRes = new Vector3D(0,0,0);

        System.out.println("Length of vector: " + vector1.vectorLength());
        System.out.println("Scalar multiplication: " + vector1.scalarMult(vector1, vector2));
        vectorRes.vectorMult(vector1, vector2, vectorRes);
        vectorRes.vectorMultPrint(vectorRes);
    }

}
