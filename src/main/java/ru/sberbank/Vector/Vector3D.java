package ru.sberbank.Vector;

public class Vector3D {
    public Vector3D(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    private float x;
    private float y;
    private float z;

    /**
     * Вычисляет длину вектора
     */
    public double vectorLength()
    {
        return Math.sqrt ( Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2) );
    }

    /**
     * Вычисляет скалярное произведение двух векторов
     * @param vector1 первый вектор
     * @param vector2 второй вектор
     */
    public double scalarMult(Vector3D vector1, Vector3D vector2)
    {
        return vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z;
    }

    /**
     * Вычисляет векторное произведение двух векторов
     * @param vector1 первый вектор
     * @param vector2 второй вектор
     * @param vectorRes результирующий вектор
     */
    public void vectorMult(Vector3D vector1, Vector3D vector2, Vector3D vectorRes)
    {
        vectorRes.x = vector1.y * vector2.z - vector1.z * vector2.y;
        vectorRes.y = vector1.z * vector2.x - vector1.x * vector2.z;
        vectorRes.z = vector1.x * vector2.y - vector1.y * vector2.x;
    }
    /**
     * Выводит на экран результаты выполнения метода {@link Vector3D#vectorMult}
     * @param vectorRes результирующий вектор
     */
    public void vectorMultPrint(Vector3D vectorRes)
    {
        System.out.println("New vector after vector multiplication: X= " + vectorRes.x + "; Y= " + vectorRes.y + "; Z= "+ vectorRes.z);
    }
}